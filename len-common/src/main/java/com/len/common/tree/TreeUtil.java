package com.len.common.tree;



import com.len.common.vo.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 节点树基础对象.扩展的话,可以继承该对象.
 * @author len
 * @date 2017/12/4
 */
public class TreeUtil {

  /**
   * 两层循环实现建树
   * 获取当前节点的两层结构对象.
   * 
   * @param treeNodes 传入的树节点列表
   * @param root 根节点树ID
   * @return
   */
  public static <T extends TreeNode> List<T> bulid(List<T> treeNodes, Object root) {

    // root节点 下直接子节点的集合
    List<T> trees = new ArrayList<>();

    for (T treeNode : treeNodes) {

      if (root.equals(treeNode.getParentId())) {
        trees.add(treeNode);
      }

      for (T it : treeNodes) {
        if (it.getParentId() == treeNode.getId()) {
          if (treeNode.getChildren() == null) {
            treeNode.setChildren(new ArrayList<>());
          }
          treeNode.add(it);
        }
      }
    }
    return trees;
  }

  /**
   * 使用递归方法建树
   * 
   * @param treeNodes
   * @return
   */
  public static <T extends TreeNode> List<T> buildByRecursive(List<T> treeNodes,Object root) {
    List<T> trees = new ArrayList<>();
    for (T treeNode : treeNodes) {
      if (root.equals(treeNode.getParentId())) {
        trees.add(findChildren(treeNode, treeNodes));
      }
    }
    return trees;
  }

  /**
   * 递归查找子节点
   * 查找当前节点treeNode的子节点的集合
   * @param treeNodes  节点的集合
   * @return
   */
  public static <T extends TreeNode> T findChildren(T treeNode, List<T> treeNodes) {
    for (T it : treeNodes) {
      if (treeNode.getId() == it.getParentId()) {
        if (treeNode.getChildren() == null) {
          treeNode.setChildren(new ArrayList<>());
        }
        treeNode.add(findChildren(it, treeNodes));
      }
    }
    return treeNode;
  }

}
