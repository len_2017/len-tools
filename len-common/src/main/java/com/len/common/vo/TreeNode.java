package com.len.common.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 节点树基础对象.扩展的话,可以继承该对象. 对树的操作,参考 com.len.common.tree.TreeUtil
 * @author len
 */
@Data
public class TreeNode {

    protected int id;
    protected int parentId;
    private List<TreeNode> children = new ArrayList<TreeNode>();

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public void add(TreeNode node){
        children.add(node);
    }


}
