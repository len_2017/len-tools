package com.len.base.rest;

import java.util.Calendar;

/**
 * @author len
 * @date 2018/1/30
 */
public class RestBaseController extends BaseController {

    private static String SUCCESS_MESSAGE = "成功";
    private static String SUCCESS_MESSAGE_CODE = "success";
    private static String FAIL_MESSAGE = "失败";
    private static String FAIL_MESSAGE_CODE = "fail";

    private static String EXCEPTION_MESSAGE = "异常";
    private static String EXCEPTION_MESSAGE_CODE = "exception";
    /**
     * 成功返回Rest对象
     * @param message  消息
     * @param messageCode 消息码
     * @param data 数据
     * @return RestReponseBody
     */
    protected RestReponseBody setSuccessRestResponse(String message, String messageCode, Object data){
        RestReponseBody<Object> responseBody = new RestReponseBody<>();
        responseBody.setSuccess(true);
        responseBody.setTimestamp(Calendar.getInstance().getTimeInMillis());
        responseBody.setMessage(message);
        responseBody.setMessageCode(messageCode);
        responseBody.setData(data);
        return  responseBody;
    }

    protected RestReponseBody setSuccessRestResponse(String message,Object data){
        return  setSuccessRestResponse(message,SUCCESS_MESSAGE_CODE,data);
    }

    protected RestReponseBody setSuccessRestResponse(Object data,String messageCode){
        return  setSuccessRestResponse(SUCCESS_MESSAGE,messageCode,data);
    }

    protected RestReponseBody setSuccessRestResponse(Object data){
        return  setSuccessRestResponse(SUCCESS_MESSAGE,SUCCESS_MESSAGE_CODE,data);
    }

    protected RestReponseBody setFailRestResponse(String message, String messageCode, Object data){
        RestReponseBody<Object> responseBody = new RestReponseBody<>();
        responseBody.setSuccess(false);
        responseBody.setTimestamp(Calendar.getInstance().getTimeInMillis());
        responseBody.setMessage(message);
        responseBody.setMessageCode(messageCode);
        responseBody.setData(data);
        return  responseBody;
    }

    protected RestReponseBody setFailRestResponse(String message,Object data){
        return  setFailRestResponse(message,FAIL_MESSAGE_CODE,data);
    }

    protected RestReponseBody setFailRestResponse(Object data,String messageCode){
        return  setFailRestResponse(FAIL_MESSAGE,messageCode,data);
    }

    protected RestReponseBody setFailRestResponse(Object data){
        return  setFailRestResponse(FAIL_MESSAGE,FAIL_MESSAGE_CODE,data);
    }

    protected RestReponseBody setExceptionRestResponse(String message, String messageCode, Object data){
        RestReponseBody<Object> responseBody = new RestReponseBody<>();
        responseBody.setSuccess(false);
        responseBody.setTimestamp(Calendar.getInstance().getTimeInMillis());
        responseBody.setMessage(message);
        responseBody.setMessageCode(messageCode);
        responseBody.setData(data);
        return  responseBody;
    }

    protected RestReponseBody setExceptionRestResponse(String message,Object data){
        return  setExceptionRestResponse(message,EXCEPTION_MESSAGE_CODE,data);
    }

    protected RestReponseBody setExceptionRestResponse(Object data,String messageCode){
        return  setExceptionRestResponse(EXCEPTION_MESSAGE,messageCode,data);
    }

    protected RestReponseBody setExceptionRestResponse(Object data){
        return  setExceptionRestResponse(EXCEPTION_MESSAGE,EXCEPTION_MESSAGE_CODE,data);
    }


}
