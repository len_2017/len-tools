package com.len.base.rest;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Rest 响应对象封装
 * @author len
 * @date 2018/1/30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class RestReponseBody<T> extends JSONObject{

    /** 数据 */
    private T data;
    /** 是否成功标识 */
    private boolean success;
    /** 响应信息 失败时必传*/
    private String message;

    /** 响应信息错误编码*/
    private String messageCode;

    /** 时间戳 */
    private long timestamp;


}
